package com.irrigation.banquemisr.dto.request;

import com.irrigation.banquemisr.domain.LandConfig;
import lombok.*;

/**
 * @author abdulmaroof
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SqsPolledMessage {
    LandConfig payload;
}
