package com.irrigation.banquemisr.scheduler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.irrigation.banquemisr.service.AppConfigService;
import com.irrigation.banquemisr.service.LandConfigService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;


/**
 * @author abdulmaroof
 */
@RequiredArgsConstructor
@Configuration
public class IrrigationJob {

    private static final Logger logger = LoggerFactory.getLogger(IrrigationJob.class);

    private final LandConfigService landConfigService;

    private final AppConfigService appConfigService;

    private final RestTemplate restTemplate;

    private final ObjectMapper objectMapper;

    @Value("${job.schedule.irrigation-enabled}")
    private boolean enabled;

    @Scheduled(cron = "${job.schedule.irrigation-cron}")
    void irrigationJobScheduler() {

        if (!enabled) {
            logger.info("Irrigation job is disabled.");
            return;
        }

        this.landConfigService.notifySensor(new ArrayList<>());
    }
}
