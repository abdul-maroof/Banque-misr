package com.irrigation.banquemisr.config;

/**
 * @author abdulmaroof
 */

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.core.region.RegionProvider;
import org.springframework.cloud.aws.core.region.StaticRegionProvider;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author Abdul Maroof
 */
@Configuration
public class AWSConfig {

    /**
     * Amazon access key.
     */
    @Value("${cloud.aws.credentials.accessKey}")
    private String amazonAccessKey;

    /**
     * Amazon secret key.
     */
    @Value("${cloud.aws.credentials.secretKey}")
    private String amazonSecretKey;

    /**
     * Amazon configured static region.
     */
    @Value("${cloud.aws.region.static}")
    private String regionName;

    /**
     * AWS Credentials Bean.
     *
     * @return AWSCredentials
     */
    @Bean(name = "AWSCredentials")
    public AWSCredentials awsCredentials() {
        return new BasicAWSCredentials(amazonAccessKey, amazonSecretKey);
    }

    @Bean(name = "AWSCredentialsProvider")
    @Primary
    public AWSCredentialsProvider awsCredentialsProvider() {
        return new AWSStaticCredentialsProvider(awsCredentials());
    }

    /**
     * Register AWS region provider.
     *
     * @return RegionProvider
     */
    @Bean(name = "AWSRegionProvider")
    public RegionProvider regionProvider() {
        return new StaticRegionProvider(regionName);
    }

    @Bean
    public QueueMessagingTemplate queueMessagingTemplate() {
        return new QueueMessagingTemplate(amazonSQSAsync());
    }

    private AmazonSQSAsync amazonSQSAsync() {
        return AmazonSQSAsyncClientBuilder.standard()
                .withCredentials(this.awsCredentialsProvider())
                .withRegion(this.regionProvider().getRegion().toString()).build();
    }

}
