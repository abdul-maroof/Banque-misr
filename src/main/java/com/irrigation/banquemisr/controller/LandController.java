package com.irrigation.banquemisr.controller;

import com.irrigation.banquemisr.domain.Land;
import com.irrigation.banquemisr.domain.LandConfig;
import com.irrigation.banquemisr.dto.request.LandConfigRequest;
import com.irrigation.banquemisr.dto.request.LandRequest;
import com.irrigation.banquemisr.exception.AutoIrrigationException;
import com.irrigation.banquemisr.service.LandConfigService;
import com.irrigation.banquemisr.service.LandService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;


/**
 * @author abdulmaroof
 */
@AllArgsConstructor
@RestController
@RequestMapping("land")
public class LandController {

    private final LandService landService;
    private final LandConfigService landConfigService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create Land")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Land created", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Land.class))})
    })
    public void createLand(@Valid @RequestBody LandRequest request) {
        landService.create(request);
    }


    @Operation(summary = "Fetch all lands")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the land", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Land.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request", content = {@Content(mediaType = "application/json")})})
    @GetMapping
    public ResponseEntity<List<Land>> fetchAllLands(Pageable pageable) {
        return ResponseEntity.ok().body(landService.list(pageable).getContent());
    }

    @PostMapping("config")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create Land Config")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Land configured", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = LandConfigRequest.class))})
    })
    public void configureLand(@Valid @RequestBody LandConfigRequest request) {
        if (Objects.isNull(request.getLandId()))
            throw new AutoIrrigationException("Land id is missing");
        landConfigService.createNewConfigForLand(request, this.landService.findById(request.getLandId()));
    }

    @PutMapping("update")
    @Operation(summary = "Update land config")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Updated land configuration", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = LandConfig.class))}),
            @ApiResponse(responseCode = "400", description = "Id not found", content = {@Content(mediaType = "application/json")})})
    public ResponseEntity updateLandConfig(@Valid @RequestBody LandConfigRequest request) {
        if (Objects.isNull(request.getId()))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Land Config id is missing");
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(landConfigService.updateLand(request));
    }
}
