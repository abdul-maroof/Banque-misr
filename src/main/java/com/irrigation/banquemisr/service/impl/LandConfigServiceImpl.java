package com.irrigation.banquemisr.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.irrigation.banquemisr.domain.Land;
import com.irrigation.banquemisr.domain.LandConfig;
import com.irrigation.banquemisr.dto.request.LandConfigRequest;
import com.irrigation.banquemisr.mapper.LandMapper;
import com.irrigation.banquemisr.repository.LandConfigHistoryRepository;
import com.irrigation.banquemisr.repository.LandConfigRepository;
import com.irrigation.banquemisr.service.AppConfigService;
import com.irrigation.banquemisr.service.LandConfigService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.webjars.NotFoundException;

import javax.transaction.Transactional;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import static com.irrigation.banquemisr.common.AppConfigKey.SENSOR_SERVICE_ENDPOINT;
import static com.irrigation.banquemisr.common.ConversionUtil.convertPojoToJsonString;
import static com.irrigation.banquemisr.common.Status.*;
import static com.irrigation.banquemisr.mapper.LandMapper.mapToLandConfig;
import static com.irrigation.banquemisr.mapper.LandMapper.mapToLandConfigHistory;

/**
 * @author abdulmaroof
 */

@Service
@AllArgsConstructor
@Transactional
public class LandConfigServiceImpl implements LandConfigService {

    private static final Logger logger = LoggerFactory.getLogger(LandConfigServiceImpl.class);

    private final ObjectMapper objectMapper;
    private final RestTemplate restTemplate;
    private final AppConfigService appConfigService;
    private final LandConfigRepository configRepository;
    private final LandConfigHistoryRepository configHistoryRepository;

    @Override
    public void createNewConfigForLand(LandConfigRequest request, Land land) {
        LandConfig landConfig = this.configRepository.save(mapToLandConfig(request, land));
        this.configHistoryRepository.save(mapToLandConfigHistory(landConfig));
    }

    @Override
    public LandConfig updateLand(LandConfigRequest request) {
        LandConfig landConfig = this.configRepository.save(mapToLandConfig(request, this.findById(request.getId()).getLand()));
        this.configHistoryRepository.save(mapToLandConfigHistory(landConfig));
        return landConfig;
    }

    @Override
    public void notifySensor(List<LandConfig> li) {
        if (li.isEmpty())
            li = this.configRepository.findByStartTimeBetweenAndStatus(LocalTime.now().minusSeconds(59), LocalTime.now(), SCHEDULED);
        li.forEach(landConfig -> {
            try {
                ResponseEntity<Map> response = restTemplate.postForEntity(this.appConfigService.findByKey(SENSOR_SERVICE_ENDPOINT), convertPojoToJsonString(landConfig, objectMapper), Map.class);
                logger.info("Successful notification sent to Sensor at: {} for: {}", ZonedDateTime.now(), response.getBody());
                landConfig.setStatus(COMPLETED);
                landConfig.setRetryCount(0L);
            } catch (HttpClientErrorException ex) {
                logger.error("Failed in sending notification to sensor at: {} due to: {}", ZonedDateTime.now(), ex.getMessage());
                landConfig.setStatus(FAILED);
                landConfig.setRetryCount(landConfig.getRetryCount());
            }
            this.saveLandConfig(landConfig);
        });
    }

    @Override
    public LandConfig findById(Long id) {
        return this.configRepository.findById(id).orElseThrow(() -> new NotFoundException("Config with this id does not exist"));
    }

    private void saveLandConfig(LandConfig request) {
        this.configRepository.save(request);
        this.configHistoryRepository.save(mapToLandConfigHistory(request));
    }

}
