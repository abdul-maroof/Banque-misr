package com.irrigation.banquemisr.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author abdulmaroof
 */
@Configuration
public class SwaggerConfig {

    @Value("${swagger.apiInfo.description}")
    String apiInfoDescription;

    @Bean
    public OpenAPI api() {
        return new OpenAPI()
                .info(new Info().title("Automatic Irrigation System Service")
                        .description(apiInfoDescription)
                        .version("1.0")
                            .contact(new Contact().email("abdulmaroofinam@gmail.com")
                                .name("Banque Misr")));
    }
}