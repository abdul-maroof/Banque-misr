package com.irrigation.banquemisr.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface MessagingService {
    void receiveMessage(Object payLoad) throws JsonProcessingException;
}
