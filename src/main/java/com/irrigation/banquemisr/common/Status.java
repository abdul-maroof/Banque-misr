package com.irrigation.banquemisr.common;

public enum Status {
    SCHEDULED,
    COMPLETED,
    FAILED
}
