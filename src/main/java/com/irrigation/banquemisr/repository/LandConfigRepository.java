package com.irrigation.banquemisr.repository;

import com.irrigation.banquemisr.common.Status;
import com.irrigation.banquemisr.domain.LandConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalTime;
import java.util.List;

public interface LandConfigRepository extends JpaRepository<LandConfig, Long> {

    List<LandConfig> findByStartTimeBetweenAndStatus(LocalTime startTime, LocalTime endTime, Status status);
}
