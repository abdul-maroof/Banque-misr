package com.irrigation.banquemisr.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author abdulmaroof
 */
@Data
@Builder
@Schema
public class LandRequest {

    @NotNull(message = "area cannot be null")
    @NotEmpty(message = "area cannot be empty")
    @NotBlank(message = "area cannot be blank")
    String area;

    @NotNull(message = "plotNumber cannot be null")
    Long plotNumber;
}
