package com.irrigation.banquemisr.service;

import com.irrigation.banquemisr.common.AppConfigKey;

public interface AppConfigService {
    String findByKey(AppConfigKey key);
}
