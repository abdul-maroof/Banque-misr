package com.irrigation.banquemisr.repository;

import com.irrigation.banquemisr.domain.Land;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LandRepository extends JpaRepository<Land, Long> {
}
