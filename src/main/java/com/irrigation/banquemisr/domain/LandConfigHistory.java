package com.irrigation.banquemisr.domain;

import com.irrigation.banquemisr.common.Status;
import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;
import java.time.ZonedDateTime;

/**
 * @author abdulmaroof
 */
@Table(name = "land_config_history")
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LandConfigHistory {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "land_id", referencedColumnName = "id")
    private Land land;

    @Column(name = "start_time", nullable = false)
    private LocalTime startTime;

    @Column(name = "water_gallons", nullable = false)
    private Long waterGallons;

    @Column(name = "duration", nullable = false)
    private Long duration;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "last_update_date", nullable = false)
    private ZonedDateTime lastUpdateDate;

    @Column(name = "retry_count", nullable = false)
    private Long retryCount;

    @Column(name = "crop_type", nullable = false)
    private String cropType;

}
