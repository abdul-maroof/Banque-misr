package com.irrigation.banquemisr.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * @author abdulmaroof
 */
@Table
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Land {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "plot_number", nullable = false)
    private Long plotNumber;

    @Column(nullable = false)
    String area;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, targetEntity = LandConfig.class, mappedBy = "land")
    private Set<LandConfig> landConfigs;

}
