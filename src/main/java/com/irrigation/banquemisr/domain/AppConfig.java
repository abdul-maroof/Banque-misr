package com.irrigation.banquemisr.domain;

import com.irrigation.banquemisr.common.AppConfigKey;
import lombok.*;

import javax.persistence.*;

/**
 * @author abdulmaroof
 */
@Table(name = "app_config")
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppConfig {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "key", nullable = false)
    private AppConfigKey key;

    @Column(name = "value", nullable = false)
    private String value;
}
