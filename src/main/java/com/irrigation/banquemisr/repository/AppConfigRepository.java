package com.irrigation.banquemisr.repository;

import com.irrigation.banquemisr.common.AppConfigKey;
import com.irrigation.banquemisr.domain.AppConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AppConfigRepository extends JpaRepository<AppConfig, Long> {
    Optional<AppConfig> findByKey(AppConfigKey name);
}
