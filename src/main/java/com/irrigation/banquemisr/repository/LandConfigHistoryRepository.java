package com.irrigation.banquemisr.repository;

import com.irrigation.banquemisr.domain.LandConfigHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LandConfigHistoryRepository extends JpaRepository<LandConfigHistory, Long> {
}
