package com.irrigation.banquemisr.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalTime;

/**
 * @author abdulmaroof
 */
@Data
@Builder
@Schema
public class LandConfigRequest {
    Long id;

    Long landId;

    @NotNull
    LocalTime time;

    @Min(value = 1)
    @Max(value = 50)
    @NotNull
    Long waterGallon;

    @Min(value = 15)
    @Max(value = 300)
    @NotNull
    Long duration;

    @NotNull
    @Size(min = 4, max = 100)
    String cropType;

    @JsonIgnore
    Long retryCount;

}
