package com.irrigation.banquemisr.service;

import com.irrigation.banquemisr.domain.Land;
import com.irrigation.banquemisr.domain.LandConfig;
import com.irrigation.banquemisr.dto.request.LandConfigRequest;

import java.util.List;

public interface LandConfigService {
    void createNewConfigForLand(LandConfigRequest request, Land land);

    LandConfig updateLand(LandConfigRequest request);

    void notifySensor(List<LandConfig> landConfig);

    LandConfig findById(Long id);
}
