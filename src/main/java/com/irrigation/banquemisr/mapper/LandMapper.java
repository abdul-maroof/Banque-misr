package com.irrigation.banquemisr.mapper;

import com.irrigation.banquemisr.domain.LandConfigHistory;
import com.irrigation.banquemisr.dto.request.LandConfigRequest;
import com.irrigation.banquemisr.domain.Land;
import com.irrigation.banquemisr.domain.LandConfig;
import com.irrigation.banquemisr.dto.request.LandRequest;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

import static com.irrigation.banquemisr.common.Status.SCHEDULED;

/**
 * @author abdulmaroof
 */
public class LandMapper {
    private LandMapper(){}

    public static Land mapToLand(LandRequest request) {
        return Land.builder()
                .area(request.getArea())
                .plotNumber(request.getPlotNumber())
                .build();
    }

    public static LandConfig mapToLandConfig(LandConfigRequest request, Land land){
        return LandConfig.builder()
                .id(request.getId())
                .cropType(request.getCropType())
                .land(land)
                .retryCount(Objects.nonNull(request.getRetryCount()) ? request.getRetryCount() : BigDecimal.ZERO.longValue())
                .duration(request.getDuration())
                .startTime(request.getTime())
                .waterGallons(request.getWaterGallon())
                .status(SCHEDULED)
                .lastUpdateDate(ZonedDateTime.now())
                .build();
    }

    public static LandConfigHistory mapToLandConfigHistory(LandConfig request) {
        return LandConfigHistory.builder()
                .id(request.getId())
                .cropType(request.getCropType())
                .retryCount(request.getRetryCount())
                .land(request.getLand())
                .duration(request.getDuration())
                .startTime(request.getStartTime())
                .waterGallons(request.getWaterGallons())
                .status(request.getStatus())
                .lastUpdateDate(ZonedDateTime.now())
                .build();
    }

}
