package com.irrigation.banquemisr.exception;

/**
 * @author abdulmaroof
 */
public class AutoIrrigationException extends RuntimeException {
    public AutoIrrigationException(String message) {
        super(message);
    }
}
