package com.irrigation.banquemisr.service.impl;

import com.irrigation.banquemisr.common.AppConfigKey;
import com.irrigation.banquemisr.repository.AppConfigRepository;
import com.irrigation.banquemisr.service.AppConfigService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AppConfigServiceImpl implements AppConfigService {

    private static final Logger logger = LoggerFactory.getLogger(AppConfigServiceImpl.class);

    private final AppConfigRepository appConfigRepository;

    public String findByKey(AppConfigKey key) {
        return this.appConfigRepository.findByKey(key).orElseGet(() -> {
            logger.error("No such element: {}", key.name());
            return null;
        }).getValue();
    }
}
