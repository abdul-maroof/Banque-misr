package com.irrigation.banquemisr.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.irrigation.banquemisr.dto.request.SqsPolledMessage;
import com.irrigation.banquemisr.service.LandConfigService;
import com.irrigation.banquemisr.service.MessagingService;
import lombok.AllArgsConstructor;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author abdulmaroof
 */
@Service
@EnableSqs
@AllArgsConstructor
public class MessagingServiceSqsImpl implements MessagingService {

    private final LandConfigService landConfigService;

    private final ObjectMapper mapper;

    @SqsListener(value = "retry-notify", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void receiveMessage(Object payLoad) throws JsonProcessingException {
        SqsPolledMessage sqsPolledMessage = mapper.readValue(payLoad.toString(), SqsPolledMessage.class);
        this.landConfigService.notifySensor(new ArrayList<>(Arrays.asList(sqsPolledMessage.getPayload())));
    }
}
