package com.irrigation.banquemisr.common;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * @author abdulmaroof
 */
public class ConversionUtil {

    ConversionUtil() {
    }

    public static Map convertPojoToJsonString(Object object, ObjectMapper mapper) {
        return mapper.convertValue(object, Map.class);
    }
}
