package com.irrigation.banquemisr.service;

import com.irrigation.banquemisr.domain.Land;
import com.irrigation.banquemisr.domain.LandConfig;
import com.irrigation.banquemisr.dto.request.LandRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LandService {
    void create(LandRequest request);

    Page<Land> list(Pageable pageable);

    void update(LandRequest land);

    Land findById(Long id);
}
