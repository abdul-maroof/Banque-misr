package com.irrigation.banquemisr.service.impl;

import com.irrigation.banquemisr.domain.Land;
import com.irrigation.banquemisr.domain.LandConfig;
import com.irrigation.banquemisr.dto.request.LandRequest;
import com.irrigation.banquemisr.mapper.LandMapper;
import com.irrigation.banquemisr.repository.LandRepository;
import com.irrigation.banquemisr.service.LandService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

/**
 * @author abdulmaroof
 */

@Service
@AllArgsConstructor
public class LandServiceImpl implements LandService {

    private final LandRepository landRepository;

    @Override
    public void create(LandRequest request) {
        this.landRepository.save(LandMapper.mapToLand(request));
    }

    @Override
    public Page<Land> list(Pageable pageable) {
        return this.landRepository.findAll(pageable);
    }

    @Override
    public void update(LandRequest land) {
        this.landRepository.save(LandMapper.mapToLand(land));
    }

    @Override
    @Cacheable(cacheNames = {"landCache"})
    public Land findById(Long id) {
        return this.landRepository.findById(id).orElseThrow(() -> new NotFoundException("Land with this id does not exist"));
    }

}
